const jwt = require("jsonwebtoken");
const auth = require("./auth");
const secret = "Capstone2";


module.exports.verifyAdmin = (req,res, callback) =>{

    let token = req.headers.authorization;

    if(token !== undefined) {

         token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return res.send({auth: "failed"});

            } else if(data.isAdmin !== true) {

                return res.send({auth: "Invalid Credentials"});
            }
            else{

                callback();
            }
        })
    }
}

module.exports.verifyConsumer = (req,res, callback) =>{

    let token = req.headers.authorization;

    if(token !== undefined) {

         token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return res.send({auth: "failed"});

            } else if(data.isAdmin === true) {

                return res.send({auth: "user not a valid customer"});
            }
            else{

                callback();
            }
        })
    }
}



module.exports.createAccessToken = (user) => {

    const data = {
        userName : user.userName,
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
        orders: user.orders
    };

    return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {

    let token = req.headers.authorization;

    if(token !== undefined) {

        //Slices the word "Bearer " to get raw token value
         token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return res.send({auth: "failed"});

            } else {

                next();
            }
        })
    } else {
        return res.send({auth:"failed"});
    }
}

module.exports.decode = (token) => {

    if(token !== undefined) {

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if(err) {

                return null;

            } else {
                return jwt.decode(token, {complete:true}).payload;
            }
        })
    } else {
        return null;
    }
}