const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const formidable = require("formidable");

// Minimum Requirements
// 6. Change product quantities
module.exports.restockProduct = (req,res) => {

	let updateProduct = { 
			stocks : req.body.quantity
	}

			console.log("preparing to restock");
		
	return Product.findByIdAndUpdate(req.params.productId, updateProduct)
				 .then(result =>{

				 	console.log("restock " + result )
				  res.send("restocked " + req.body.quantity + " units")
				});

}

// Minimum Requirements
// 5. Added Products



// Minimum Requirements
// 3. Create Product (Admin only)


const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);

        fileReader.onload = () => {
            resolve(fileReader.result);
        };

        fileReader.onerror = (error) => {
            reject(error);
        };
    });
};

function parseMultipart (req) {

	return new Promise((resolve, reject) => {
		const form = formidable({multiplies: true});

		form.parse(req,(error, fields, files) => {
			if(error){
				reject(error)
				return;
			}
			resolve({...fields, ...files});
		});
	});
}

module.exports.createProduct = (req,res) => {

 	// let newProduct;

	// parseMultipart(req)
	// 	.then(data => {
	// 		    console.log(JSON.stringify(data.preview));
	// 		    newProduct = new Product({
	// 			    	name: data.name,
	// 			    	description: data.description,
	// 			    	price: data.price,
	// 			    	stocks: data.stocks,
	// 			    	category: data.category,
	// 			    	preview: Buffer.from(JSON.stringify(data.preview)).toString('base64')
	// 			    	});

	// 	})
	// 	.finally(result => {

	//       return newProduct.save().then(product => {
	// 	            res.send("product added successfully");
	// 	       })
	// 	       .catch(error => {
	// 	            res.send(error);
	// 	        });

	// 	})
		


	// return;
	
    let newProduct = new Product({

	    name: req.body.name,
	    description: req.body.description,
    	price: req.body.price,
    	stocks: req.body.stocks,
    	category: req.body.category//,
    	//preview: Buffer.from(JSON.stringify(data.preview)).toString('base64')

	    });

	      return newProduct.save().then(product => {
	            res.send(newProduct);
	       })
	       .catch(error => {
	            res.send(error);
	        });
}


// Minimum Requirements
// 6. Update Product information (Admin only)
module.exports.updateProduct = (req,res) => {

	console.log("preparing to update");
		let updateProduct = {
			name : req.body.name,
			description : req.body.description,
			price : req.body.price,
			stocks : req.body.stocks,
			isActive : req.body.isActive,
			category : req.body.category,
		};

	return Product.findByIdAndUpdate(req.params.productId, updateProduct)
				 .then(result =>{

				 	console.log("archive " + result )
				  res.send(updateProduct)
				});
}





// Minimum Requirements
// 5. Retrieve single product
module.exports.viewProduct = (req,res) => {
		return Product.findById(req.params.productId).then(result => res.send(result));
}

// Minimum Requirements
// 4. Retrieve all active products
module.exports.viewAllProduct = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}

module.exports.viewCategoryProduct = (req, res) => {

	return Product.find({isActive: true, category: req.params.category}).then(result => res.send(result));
}


module.exports.viewAllProductAdmin = (req, res) => {
	return Product.find({}).then(result => res.send(result));
}





// Minimum Requirements
// 7. Archive Product (Admin only)
module.exports.archiveProduct = (req,res) => {
 	
 	console.log("preparing to archive");
		let updateProduct = {
			isActive : false
		};


	return Product.findByIdAndUpdate(req.params.productId, updateProduct)
				 .then(result =>{

				 	console.log("archive " + result )
				  res.send(updateProduct)
				});

}

module.exports.checkoutOrder = async (req,res) => {

    const userId = auth.decode(req.headers.authorization).id;
	let userData = await User.findById(userId)
			.then(result => {
			result.orders = AddEachCartToOrder(result.cart, result.orders);
		    result.cart = [];
			console.log(result);
	    return result;
	})

 	return User.findByIdAndUpdate(userData.id, userData)
 		.then(result => res.send(result));
}


// Stretch Goals
// 4. Add to Cart
module.exports.addToCart = async (req,res) => {
	
    const userId = auth.decode(req.headers.authorization).id;
    let product = await Product.findById(req.params.productId)


	let userData = await User.findById(userId)
			.then(result => {

		  	result.cart.push( 
		    {		
   		    		  productId: req.params.productId,
		              productName: product.name,
		              quantity: req.body.quantity,
    				  isTrial : req.body.isTrial,
					  total : product.price * req.body.quantity
		    })


	    return result;
	})

	return User.findByIdAndUpdate(userData.id, userData)
		.then(result => res.send(result));
	      


}

module.exports.ChangeStocks = async (req,res) => {
	
    const userId = auth.decode(req.headers.authorization).id;
    let product = await Product.findById(req.params.productId)

	let updatedStock = {
            stocks : product.stocks + req.body.quantity 
    };

	return Product.findByIdAndUpdate(product.id, updatedStock)
		.then(result => res.send(result));


}





// Stretch Goals
// 4. Add to Cart
module.exports.remoteFromCart = async (req,res) => {

    const userId = auth.decode(req.headers.authorization).id;
    let product = await Product.findById(req.params.productId)

    let cart = {
    			productName : product.name,
    			quantity : req.body.quantity,
    			isTrial : req.body.isTrial,
    		};

 	let userData = await User.findById(userId)
 			.then(result => {

 			let indexOfCart = GetIndexOf(result.cart, cart);
 			console.log(indexOfCart)
 			if(indexOfCart !== -1)
	 			result.cart.splice(indexOfCart,1);

 	    return result;
 	})

	return User.findByIdAndUpdate(userData.id, userData)
		.then(result => res.send(result));

}

const AddEachCartToOrder = (cart, order) => {

	cart.forEach((item) => {
		order.push(item);
	})


	return order;
}


const GetIndexOf = (collection, product) => {

	let index = -1;
	let storedValue = -1;

	collection.forEach((item) => {

		index ++;

		if(item.productName === product.productName && item.quantity === product.quantity && item.isTrial === product.isTrial)
		{
			console.log("found at " + index)
			storedValue = index;
		}

	} );


	return storedValue;
}

// Stretch Goals
// 9. Total price for all items
module.exports.getTotalPrice = async (req,res) => {
 	const userId = auth.decode(req.headers.authorization).id;
    return await User.findById(userId)
		.then(result => {
			let total = 0;

			result.cart.forEach(async (item) => {	
					let product = await Product.findById(item.productId)
					total += calculateSubtotal(item, product);

			})
				console.log(total);
				res.send(total)
			
		})

}

// Stretch Goals
// 8. Subtotal for each item
module.exports.subtotalOrderItems = async (req,res) => {

    const userId = auth.decode(req.headers.authorization).id;
    let product = await Product.findById(req.params.productId)

    return await User.findById(userId)
		.then(result => {
			let subtotal = 0;
			result.cart.forEach((item) => {
				if(item.productId === req.params.productId)
					subtotal += calculateSubtotal(item, product);

			})
			return subtotal;

			
		})

}

const calculateSubtotal =  (cart, product) => {
		console.log(product);
	let total = 0;
		total += cart.quantity * product.price
	return total;
}



// Stretch Goals
// 3. Retrieve all orders (Admin only)
module.exports.viewAllOrder = (req,res) => {
 	
}

// Stretch Goals
// 2. Retrieve authenticated user’s orders
module.exports.viewUserOrders = (req,res) => {
 		
    const userId = auth.decode(req.headers.authorization).id;
	return User.findById(userId)
		.then(result => res.send(result));
}



// Minimum Requirements
// 1. User registration
// 2. User authentication
// 3. Create Product (Admin only)
// 4. Retrieve all active products
// 5. Retrieve single product
// 6. Update Product information (Admin only)
// 7. Archive Product (Admin only)
// 8. Non-admin User checkout (Create Order)
// 9. Retrieve User Details