const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Minimum Requirements
// 1. User registration
module.exports.register = (req,res) => {
    let newUser = new User({
       userName : req.body.userName,
       email: req.body.email,
       password: bcrypt.hashSync(req.body.password, 10),
    });

    return newUser.save().then(user => {
        console.log(user);

        //Send authy code and auto login user   
        return res.send({accessToken: auth.createAccessToken(newUser)});
    })
    .catch(error => {
        console.log(error);
        res.send(false);
    })
}

// Minimum Requirements
// 2. User authentication
module.exports.authenticateUser = (req, res) => {

      const userData = auth.decode(req.headers.authorization);

      return User.findOne({userName: req.body.userName}).then(result => {

        if(result == null) {

            return res.send({message: "No user found"});

        } else {

        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordCorrect) {

                return res.send({accessToken: auth.createAccessToken(result)});

            } else {
                return res.send(false)
            }
        }
    })
  }

// Minimum Requirements
// 9. Retrieve User Details
module.exports.retrieveUserDetails = (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    //TODO figure out why decode didnt return username
    return User.findById(userData.id).then(result => {
         if(result == null) {

            return res.send({message: "No user found"});

        } else {

            console.log(result);      
            return res.send(result);
        }
    })
}

module.exports.changePassword = (req, res) => {


    let updatedUser = {
        password : bcrypt.hashSync(req.body.newPassword, 10)
    }

    return User.findByIdAndUpdate(req.body.userId, updatedUser)
                 .then(result => 
                 {      
                    return res.send({accessToken: auth.createAccessToken(result)});
                });
   
}

module.exports.viewUserDetails = (req, res) => {

      return User.findById(req.params.id).then(result => {

        console.log(result)

        if(result == null) {

            return res.send({message: "No user found"});

        } else {
            //view only public info

         let returnUserData = new User({
               userName : result.userName,
               email: result.email,
               isAdmin : result.isAdmin,
            });

            return res.send(returnUserData)
        }
    })
  }

// Stretch Goals
// 1. Set user as admin (Admin only)
module.exports.setAdmin = (req,res) => {
  
        let updatedUser = {

            isAdmin : true

        };
        return User.findByIdAndUpdate(req.params.id, updatedUser)
                 .then(result => 
                 {
                    res.send(result);
                });
   
    
}


module.exports.getAllNonAdminUsers = (req,res) => {
    
    return User.find({isAdmin:false}).then(result => res.send(result));
}

module.exports.createOrder = async (req, res) => {

    //Get all the orders under userId

    let product = await Product.findById(req.params.productId)
                 .then(result => 
                 {
                    return result;
                });

    // const userData = await auth.decode(req.headers.authorization);

    // console.log(userData);
    // console.log(userData.orders);
    // console.log(product);


    // userData.orders.push( 
    //  {
    //         productName: product.name,
    //         quantity: req.body.quantity,
    //     })


    // return User.findByIdAndUpdate(userData.id, userData)
    //          .then(result =>{

    //             console.log("order " + result )
    //           res.send(result)
    //         });


    const userId = auth.decode(req.headers.authorization).id;
    let userData = await User.findById(userId).then(result => {
        result.orders.push( 
           {
                  productName: product.name,
                  quantity: req.body.quantity,
          })

        return result;
    })

    return User.findByIdAndUpdate(userData.id, userData)
             .then(result =>{

                console.log("order " + result )
              res.send(result)
            });


           

}



module.exports.getAllUserOrders = (req, res) => {

    return User.find({isAdmin:false})
    .then(result => res.send(result))

}






module.exports.addCurrency = (req, res) => {

    return User.findById(req.params.id)
             .then(result => 
             {
                currentBalance = result.balance;
                let updatedUser = {
                    balance : req.body.balance + currentBalance

                };

                return User.save().then(userData => {
                      console.log(userData);
                      res.send(true);
                 })
                 .catch(error => {
                      console.log(error)
                      res.send(false);
                  });
            });


}



///Extra
module.exports.demoProduct = (req,res) => {
    
}