const express = require('express');
const mongoose = require('mongoose');

const app = express();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

//MongoDB Connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt.veho36n.mongodb.net/b270-capstone2?retryWrites=true&w=majority",
                {
                    useNewUrlParser : true,
                    useUnifiedTopology: true,
                });

//Set notif for db connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connection to DB Secured!"));

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());


app.use("/", userRoutes);
app.use("/", productRoutes);




app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
});

