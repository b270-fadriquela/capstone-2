const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

    name: {

        type: String,
        required: [true, "product name is required"]

    },

    description: {
        type: String,
        required: [true, "Description is required"]
    },

    price : {
        type: Number,
        required: [true, "Price is required"]
    },

    stocks : {
        type: Number,
        required: [true, "product stocks is required"]
    },

    createdOn : {
        type: Date,
        default:new Date()
    },

    isActive : {
        type: Boolean,
        default:true
    },


    category : {
        type: String,
        required: [true, "category is required"]
    },
    isFeatured : {
        type: Boolean,
        default:false
    },
    featuredBanner : {
        type: String
    },
    preview : {
        type: String
    }

})

module.exports = mongoose.model("Product", courseSchema);