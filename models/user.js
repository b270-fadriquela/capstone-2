const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    userName:{

        type: String,
        required: [true, "userName is required"] 

    },

    email: {

        type: String,
        required: [true, "Email is required"] 
    },

    password : {
        type: String,
        required: [true, "password is required"]
    },

    isAdmin : {
        type: Boolean,
        default: false
    },

    balance: {
        type: Number//,
        // default: 0
    },

    cart : [
        { 
                productId: {
                    type: String,
                    required: [true, "product quantity is required"] 
                },
                productName: {
                    type: String,
                    required: [true, "product name is required"] 
                },
                quantity: {
                    type: Number,
                    required: [true, "product quantity is required"] 
                },

                isTrial: {
                    type: Boolean,
                    default: false
                },
                total: {
                    type: Number,
                    required: [true, "product total is required"] 
                }
        }
    ],

    orders : [
        {
                productName: {

                    type: String,
                    required: [true, "product name is required"] 
                },
                quantity: {
                    type: Number,
                    required: [true, "product quantity is required"] 
                },

                purchaseDate:  {
                    type: Date,
                    default: new Date()
                },

                isTrial: {

                    type: Boolean,
                    default: false

                }

            }


         

        
    ]


})

module.exports = mongoose.model("Userbase", userSchema);

