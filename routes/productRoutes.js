const express = require("express");
const router = express.Router();
const productController = require("../controller/productController");
const authy = require("../auth.js");

router.post("/product", authy.verifyAdmin, productController.createProduct);
router.patch("/product/:productId", authy.verifyAdmin, productController.updateProduct);
router.get("/product/:productId", productController.viewProduct);
router.get("/products", productController.viewAllProduct);
router.get("/:category", productController.viewCategoryProduct);

router.get("/products/all", authy.verifyAdmin, productController.viewAllProductAdmin);

router.patch("/product/:productId/archive", authy.verifyAdmin, productController.archiveProduct);


router.patch("/product/:productId/add", authy.verifyConsumer, productController.addToCart);
router.patch("/product/:productId/remove", authy.verifyConsumer, productController.remoteFromCart);
router.patch("/product/:productId/stocks", authy.verifyConsumer, productController.ChangeStocks);

router.patch("/checkout", authy.verifyConsumer, productController.checkoutOrder);

router.get("/history", authy.verifyConsumer, productController.viewUserOrders);

router.patch("/restock/:productId", authy.verifyAdmin, productController.restockProduct)


// router.get("/checkout", authy.verifyConsumer, productController.Checkout);




// // router.post("/product", authy.verifyConsumer, productController.addNewProduct);
// router.get("/view/:productId/purchase", authy.verify, productController.addToCart);
// router.patch("/view/:productId/demo", authy.verify, productController.demoProduct);


module.exports = router;

