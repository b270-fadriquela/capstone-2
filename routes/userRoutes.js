const express = require("express");
const router = express.Router();
const userController = require("../controller/userController");
const authy = require("../auth.js");

router.post("/login", userController.authenticateUser);
router.post("/signup",(req,res) => {userController.register(req,res)});


router.get("/profile/", authy.verify, userController.retrieveUserDetails);
router.patch("/profile/changePassword", authy.verify, userController.changePassword);
router.get("/profile/:id", userController.viewUserDetails);

router.get("/users/all", authy.verifyAdmin, userController.getAllNonAdminUsers);

router.patch("/promote/:id", authy.verifyAdmin, userController.setAdmin);



//router.put("/purchase",  authy.verifyConsumer, userController.addToCart);

//Add verification somehow? send to email?
//router.post("/forgetPassword", userController.recoverPassword);


module.exports = router;